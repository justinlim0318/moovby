package com.moovby.moovby.di.component;

import android.app.Application;
import android.content.Context;

import com.moovby.moovby.di.module.ApiModule;
import com.moovby.moovby.di.module.ApplicationModule;
import com.moovby.moovby.di.qualifier.ApplicationContext;
import com.moovby.moovby.di.scope.ApplicationScope;
import com.moovby.moovby.network.IApiHelper;
import com.moovby.moovby.preference.IPreference;

import dagger.BindsInstance;
import dagger.Component;

@ApplicationScope
@Component(modules = {
        ApiModule.class,
        ApplicationModule.class,
        })
public interface ApplicationComponent {

    void inject(Application app);

    @ApplicationContext
    Context context();

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        Builder api(ApiModule apiModule);

        ApplicationComponent build();
    }

    Application application();

    IApiHelper getApiHelper();

    IPreference getPreferenceHelper();


}
