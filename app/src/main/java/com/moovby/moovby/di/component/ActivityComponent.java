package com.moovby.moovby.di.component;

import com.moovby.moovby.base.BaseActivity;
import com.moovby.moovby.di.module.ActivityModule;
import com.moovby.moovby.di.scope.ActivityScope;
import com.moovby.moovby.signin.SigninActivity;
import com.moovby.moovby.vehicleDetails.VehicleDetailActivity;

import dagger.Component;

@ActivityScope
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface
ActivityComponent {

    void inject(BaseActivity activity);

    void inject(SigninActivity activity);

    void inject(VehicleDetailActivity activity);
}
