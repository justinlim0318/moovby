package com.moovby.moovby.di.component;

import com.moovby.moovby.base.BaseFragment;
import com.moovby.moovby.di.module.FragmentModule;
import com.moovby.moovby.di.scope.FragmentScope;
import com.moovby.moovby.vehicleList.VehichlesFragment;

import dagger.Component;

@FragmentScope
@Component(dependencies = ApplicationComponent.class, modules = FragmentModule.class)
public interface FragmentComponent {
    void inject(BaseFragment baseFragment);

    void inject(VehichlesFragment vehichlesFragment);

}
