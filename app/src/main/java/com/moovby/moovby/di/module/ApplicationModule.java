package com.moovby.moovby.di.module;

import android.app.Application;
import android.content.Context;

import com.moovby.moovby.di.qualifier.ApplicationContext;
import com.moovby.moovby.di.scope.ApplicationScope;
import com.moovby.moovby.preference.IPreference;
import com.moovby.moovby.preference.PreferenceHelper;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class ApplicationModule {

    @Binds
    @ApplicationContext
    public abstract Context provideContext(Application application);

    @Provides
    @ApplicationScope
    static IPreference providePreferencesHelper(PreferenceHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }
}
