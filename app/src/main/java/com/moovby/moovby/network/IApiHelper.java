package com.moovby.moovby.network;


import com.moovby.moovby.model.SignInRequestModel;
import com.moovby.moovby.model.SignInResponseModel;
import com.moovby.moovby.model.VehicleDetailResponseModel;
import com.moovby.moovby.model.VehiclesResponseModel;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IApiHelper {

    @POST("signin")
    Observable<SignInResponseModel> signIn(@Body SignInRequestModel signInRequestModel);

    @GET("vehicles")
    Observable<VehiclesResponseModel> fetchVehichles(@Header("Authorization") String token,
                                                     @Query("longitude") double longitude,
                                                     @Query("latitude") double latitude);

    @GET("vehicles/{id}")
    Observable<VehicleDetailResponseModel> fetchVehicleDetail(@Header("Authorization") String token,
                                                              @Path("id") String id);


    class Factory {
        public static IApiHelper create(Retrofit retrofit) {

            return retrofit.create(IApiHelper.class);
        }
    }
}
