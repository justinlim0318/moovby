package com.moovby.moovby.network;

import android.content.Context;

import com.moovby.moovby.di.qualifier.ApplicationContext;
import com.moovby.moovby.di.scope.ApplicationScope;
import com.moovby.moovby.model.SignInRequestModel;
import com.moovby.moovby.model.SignInResponseModel;
import com.moovby.moovby.model.VehicleDetailResponseModel;
import com.moovby.moovby.model.VehiclesResponseModel;

import javax.inject.Inject;

import io.reactivex.Observable;

@ApplicationScope
public class ApiManager implements IApiHelper {
    private IApiHelper apiHelper;
    private Context context;

    @Inject
    public ApiManager(@ApplicationContext Context context,
                      IApiHelper apiHelper) {
        this.apiHelper = apiHelper;
        this.context = context;
    }


    @Override
    public Observable<SignInResponseModel> signIn(SignInRequestModel signInRequestModel) {
        return apiHelper.signIn(signInRequestModel);
    }

    @Override
    public Observable<VehiclesResponseModel> fetchVehichles(String token, double longitude, double latitude) {
        return apiHelper.fetchVehichles(token, longitude, latitude);
    }

    @Override
    public Observable<VehicleDetailResponseModel> fetchVehicleDetail(String token, String id) {
        return apiHelper.fetchVehicleDetail(token, id);
    }
}
