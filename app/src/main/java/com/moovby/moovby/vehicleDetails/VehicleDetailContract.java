package com.moovby.moovby.vehicleDetails;

import com.moovby.moovby.base.BasePresenter;
import com.moovby.moovby.base.BaseView;

public class VehicleDetailContract {
    interface View extends BaseView {

        void showCoverPhoto(String url);

        void showBanner(String banner_url);

        void showDescription(String description);

        void showHostName(String name);

        void showAvatar(String avatar);

        void showRating(int rating);

        void showModelName(String modelName);

        void showPrice(String price);

    }

    interface Presenter extends BasePresenter {

        void fetchVehicleDetail(String id);

    }
}
