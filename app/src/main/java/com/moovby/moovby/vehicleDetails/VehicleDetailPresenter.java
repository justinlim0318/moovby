package com.moovby.moovby.vehicleDetails;

import com.moovby.moovby.base.BaseMvpPresenter;
import com.moovby.moovby.model.HostModel;
import com.moovby.moovby.model.Rates;
import com.moovby.moovby.model.VehicleModel;
import com.moovby.moovby.network.IApiHelper;
import com.moovby.moovby.preference.IPreference;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class VehicleDetailPresenter extends BaseMvpPresenter<VehicleDetailContract.View> implements VehicleDetailContract.Presenter {

    @Inject
    public VehicleDetailPresenter(IApiHelper iApiHelper, IPreference preference) {
        super(iApiHelper, preference);
    }

    @Override
    public void fetchVehicleDetail(String id) {
        String token = "Bearer " + preferenceHelper.getUserToken();
        addToSubscription(apiHelper.fetchVehicleDetail(token, id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseModel -> {
                            initView(responseModel.getVehicleData());
                        },
                        error -> {

                        }));

    }

    private void initView(VehicleModel vehicleModel) {
        if (vehicleModel == null) {
            return;
        }
        if (vehicleModel.getModelName() != null) {
            getView().showModelName(vehicleModel.getModelName());
        }
        VehicleModel.Images images = vehicleModel.getImages().get(0);
        if (images != null) {
            getView().showCoverPhoto(images.getOriginal());
        }
        if (vehicleModel.getBanner() != null) {
            getView().showBanner(vehicleModel.getBanner().getBanner_url());
        }
        if (vehicleModel.getDescription() != null) {
            getView().showDescription(vehicleModel.getDescription());
        }
        HostModel hostModel = vehicleModel.getHost();
        if (hostModel != null) {
            getView().showHostName(hostModel.getName());
            getView().showAvatar(hostModel.getAvatar());
        }
        getView().showRating(vehicleModel.getRating());
        Rates rates = vehicleModel.getRates();
        if (rates != null) {
            getView().showPrice("RM " + String.valueOf(rates.getHourlyRate()) + "\n per hour");
        }


    }
}
