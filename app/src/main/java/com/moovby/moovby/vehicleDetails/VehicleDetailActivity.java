package com.moovby.moovby.vehicleDetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.moovby.moovby.R;
import com.moovby.moovby.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VehicleDetailActivity extends BaseActivity implements VehicleDetailContract.View {
    @Inject
    VehicleDetailPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_cover)
    ImageView ivCover;
    @BindView(R.id.collapse_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.iv_avatar)
    ImageView ivAvatar;
    @BindView(R.id.iv_banner)
    ImageView ivBanner;
    @BindView(R.id.tv_host_name)
    TextView tvHostName;
    @BindView(R.id.tv_desc)
    TextView tvDesc;
    @BindView(R.id.smallRatingBar)
    RatingBar ratingBar;
    @BindView(R.id.tv_host)
    TextView tvHost;
    @BindView(R.id.tv_model)
    TextView tvModel;
    @BindView(R.id.tv_price)
    TextView tvPrice;

    public static final String EXTRA_ID = "EXTRA_ID";

    public static void startDetailActivity(Context activity, String id) {
        Intent intent = new Intent(activity, VehicleDetailActivity.class);
        intent.putExtra(EXTRA_ID, id);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getActivityComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter.onAttachView(this);
        initIntent();

    }

    private void initIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String id = bundle.getString(EXTRA_ID);
            presenter.fetchVehicleDetail(id);
        }
    }

    @Override
    public void showCoverPhoto(String url) {
        Glide.with(this)
                .load(url)
                .into(ivCover);
    }

    @Override
    public void showBanner(String banner_url) {
        ivBanner.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(banner_url)
                .into(ivBanner);
    }

    @Override
    public void showDescription(String description) {
        tvDesc.setText(description);
    }

    @Override
    public void showHostName(String name) {
        tvHostName.setText(name);
        tvHost.setText(name);
    }

    @Override
    public void showAvatar(String avatar) {
        Glide.with(this)
                .load(avatar)
                .into(ivAvatar);
    }

    @Override
    public void showRating(int rating) {
        ratingBar.setRating(rating);
    }

    @Override
    public void showModelName(String modelName) {
        tvModel.setText(modelName);
        collapsingToolbarLayout.setTitle(modelName);
    }

    @Override
    public void showPrice(String price) {
        tvPrice.setText(price);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
