package com.moovby.moovby.base;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.moovby.moovby.MoovbyApplication;
import com.moovby.moovby.di.component.DaggerFragmentComponent;
import com.moovby.moovby.di.component.FragmentComponent;
import com.moovby.moovby.network.IApiHelper;
import com.moovby.moovby.preference.IPreference;

import javax.inject.Inject;

public class BaseFragment extends Fragment {
    private FragmentComponent mFragmentComponent;

    @Inject
    IPreference preference;
    @Inject
    IApiHelper apiHelper;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getFragmentComponent().inject(this);
    }

    public FragmentComponent getFragmentComponent() {
        if (mFragmentComponent == null) {
            mFragmentComponent = DaggerFragmentComponent.builder()
                    .applicationComponent(MoovbyApplication.getApplicationComponent())
                    .build();
        }
        return mFragmentComponent;
    }


}
