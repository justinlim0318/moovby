package com.moovby.moovby.base;

import android.support.annotation.NonNull;

import com.moovby.moovby.network.IApiHelper;
import com.moovby.moovby.preference.IPreference;

import java.lang.ref.WeakReference;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BaseMvpPresenter<V extends BaseView> implements BasePresenter {

    protected IApiHelper apiHelper;
    protected IPreference preferenceHelper;
    private WeakReference<V> mViewWeak = new WeakReference<>(null);
    protected CompositeDisposable disposableSubscription = new CompositeDisposable();


    public BaseMvpPresenter(IApiHelper iApiHelper, IPreference iPreference) {
        this.apiHelper = iApiHelper;
        this.preferenceHelper = iPreference;
    }

    public void onAttachView(V view) {
        mViewWeak = new WeakReference<>(view);
    }

    @NonNull
    public V getView() {
        return mViewWeak.get();
    }

    public void addToSubscription(Disposable disposable) {
        disposableSubscription.add(disposable);
    }

    public void onDestroyView() {
        disposableSubscription.clear();
    }
}
