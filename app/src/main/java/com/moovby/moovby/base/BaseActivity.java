package com.moovby.moovby.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.moovby.moovby.MoovbyApplication;
import com.moovby.moovby.di.component.ActivityComponent;
import com.moovby.moovby.di.component.DaggerActivityComponent;
import com.moovby.moovby.network.IApiHelper;
import com.moovby.moovby.preference.IPreference;

import javax.inject.Inject;

public abstract class BaseActivity extends AppCompatActivity {
    @Inject
    protected IApiHelper apiHelper;
    @Inject
    protected IPreference preference;

    public ActivityComponent mActivityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getActivityComponent().inject(this);
        super.onCreate(savedInstanceState);
    }


    public ActivityComponent getActivityComponent() {
        if (mActivityComponent == null) {
            mActivityComponent = DaggerActivityComponent.builder().
                    applicationComponent(MoovbyApplication.getApplicationComponent())
                    .build();
        }
        return mActivityComponent;
    }

}
