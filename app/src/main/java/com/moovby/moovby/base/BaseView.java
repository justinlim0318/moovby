package com.moovby.moovby.base;

public interface BaseView {
    void showLoading();

    void hideLoading();
}
