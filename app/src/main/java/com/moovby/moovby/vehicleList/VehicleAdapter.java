package com.moovby.moovby.vehicleList;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.moovby.moovby.R;
import com.moovby.moovby.model.VehicleModel;
import com.moovby.moovby.vehicleDetails.VehicleDetailActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.MyViewHolder> {

    private ArrayList<VehicleModel> vehicleModels;


    public VehicleAdapter(ArrayList<VehicleModel> vehicleModels) {
        this.vehicleModels = vehicleModels;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_vehicle, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.onBind(vehicleModels.get(position));
    }

    @Override
    public int getItemCount() {
        return vehicleModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_distance)
        TextView tvDistance;
        @BindView(R.id.tv_model)
        TextView tvModel;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_trip)
        TextView tvTrip;
        @BindView(R.id.iv_cover)
        ImageView ivCover;
        @BindView(R.id.card_view)
        ConstraintLayout layout;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void onBind(VehicleModel vehicleModel) {
            tvDistance.setText(vehicleModel.getDistance());
            tvModel.setText(vehicleModel.getModelName());
            String price = "RM " + String.valueOf(vehicleModel.getHourlyRate());
            tvPrice.setText(price);
            Glide.with(itemView.getContext())
                    .load(vehicleModel.getImages().get(0).getOriginal())
                    .into(ivCover);
            String trips = vehicleModel.getTrips() != 0 ? String.valueOf(vehicleModel.getTrips()) + "trips"
                    : itemView.getContext().getString(R.string.text_no_trips);
            tvTrip.setText(trips);
            layout.setOnClickListener(v -> VehicleDetailActivity.startDetailActivity(itemView.getContext(), vehicleModel.getId()));
        }
    }

}
