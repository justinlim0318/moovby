package com.moovby.moovby.vehicleList;

import com.moovby.moovby.base.BasePresenter;
import com.moovby.moovby.base.BaseView;
import com.moovby.moovby.model.VehicleModel;

import java.util.ArrayList;

public interface VehiclesContract {
    interface View extends BaseView {

        void showVehicleList(ArrayList<VehicleModel> vehicles);
    }

    interface Presenter extends BasePresenter {
        void fetchVehicles();

    }
}
