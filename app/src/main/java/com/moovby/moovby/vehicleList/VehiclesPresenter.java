package com.moovby.moovby.vehicleList;

import com.moovby.moovby.base.BaseMvpPresenter;
import com.moovby.moovby.network.IApiHelper;
import com.moovby.moovby.preference.IPreference;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class VehiclesPresenter extends BaseMvpPresenter<VehiclesContract.View> implements VehiclesContract.Presenter {

    @Inject
    public VehiclesPresenter(IApiHelper iApiHelper, IPreference iPreference) {
        super(iApiHelper, iPreference);
    }

    @Override
    public void fetchVehicles() {
        String token = "Bearer " + preferenceHelper.getUserToken();
        addToSubscription(apiHelper.fetchVehichles(token, 101.6669844, 3.087857)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseModel -> {
                            getView().showVehicleList(responseModel.getVehicles());

                        },
                        error -> {

                        }));
    }
}
