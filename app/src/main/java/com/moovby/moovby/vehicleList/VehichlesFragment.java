package com.moovby.moovby.vehicleList;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moovby.moovby.R;
import com.moovby.moovby.base.BaseFragment;
import com.moovby.moovby.model.VehicleModel;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VehichlesFragment extends BaseFragment implements VehiclesContract.View {
    @Inject
    VehiclesPresenter presenter;

    @BindView(R.id.rv_vehicles)
    RecyclerView recyclerView;

    public static VehichlesFragment newInstance() {
        VehichlesFragment fragment = new VehichlesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_vehicles, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getFragmentComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.onAttachView(this);
        ButterKnife.bind(this, view);
        presenter.fetchVehicles();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showVehicleList(ArrayList<VehicleModel> vehicles) {
        VehicleAdapter adapter = new VehicleAdapter(vehicles);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }
}
