package com.moovby.moovby.signin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.moovby.moovby.MainActivity;
import com.moovby.moovby.R;
import com.moovby.moovby.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SigninActivity extends BaseActivity implements SignInContract.View {

    @Inject
    SigninPresenter presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btn_sign_in)
    Button btnSignin;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.tl_email)
    TextInputLayout tlEmail;
    @BindView(R.id.tl_password)
    TextInputLayout tlPassword;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getActivityComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        presenter.onAttachView(this);
        btnSignin.setOnClickListener(v -> presenter.signIn(etEmail.getText().toString(), etPassword.getText().toString()));
        setSupportActionBar(toolbar);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showEmailError() {
        tlEmail.setError(getString(R.string.error_email));
    }

    @Override
    public void showPasswordError() {
        tlPassword.setError(getString(R.string.error_password));
    }

    @Override
    public void signInSuccess() {
        MainActivity.startMainActivity(this);
    }

    @Override
    public void signInFailed() {
        Toast.makeText(this, R.string.error_sign_in, Toast.LENGTH_SHORT).show();
    }
}
