package com.moovby.moovby.signin;

import android.text.TextUtils;

import com.moovby.moovby.base.BaseMvpPresenter;
import com.moovby.moovby.model.SignInRequestModel;
import com.moovby.moovby.network.IApiHelper;
import com.moovby.moovby.preference.IPreference;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SigninPresenter extends BaseMvpPresenter<SignInContract.View> implements SignInContract.Presenter {

    @Inject
    public SigninPresenter(IApiHelper iApiHelper, IPreference preference) {
        super(iApiHelper, preference);
    }


    @Override
    public void signIn(String email, String password) {
        if (TextUtils.isEmpty(email)) {
            getView().showEmailError();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            getView().showPasswordError();
            return;
        }
        SignInRequestModel requestModel = new SignInRequestModel(email, password);
        addToSubscription(apiHelper.signIn(requestModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(signInResponseModel -> {
                            preferenceHelper.setUserToken(signInResponseModel.getAuthToken());
                            getView().signInSuccess();
                        },
                        error -> getView().signInFailed()));
    }
}
