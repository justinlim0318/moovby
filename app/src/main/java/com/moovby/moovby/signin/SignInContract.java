package com.moovby.moovby.signin;

import com.moovby.moovby.base.BasePresenter;
import com.moovby.moovby.base.BaseView;

public interface SignInContract {

    interface View extends BaseView {
        void showEmailError();

        void showPasswordError();

        void signInSuccess();

        void signInFailed();

    }

    interface Presenter extends BasePresenter {
        void signIn(String email, String password);

    }
}
