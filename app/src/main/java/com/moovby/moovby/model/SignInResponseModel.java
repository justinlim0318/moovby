package com.moovby.moovby.model;

public class SignInResponseModel extends CommonResponseModel {
    private String auth_token;
    private UserResponseModel user;


    public String getAuthToken() {
        return auth_token;
    }

    public UserResponseModel getUser() {
        return user;
    }
}
