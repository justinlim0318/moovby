package com.moovby.moovby.model;

public class DeviceInfo {
    private String device_identifier;
    private String device_information;
    private String device_type;
    private String onesignal_player_id;

    public DeviceInfo(String device_identifier, String device_information, String device_type, String onesignal_player_id) {
        this.device_identifier = device_identifier;
        this.device_information = device_information;
        this.device_type = device_type;
        this.onesignal_player_id = onesignal_player_id;
    }

    public String getDeviceIdentifier() {
        return device_identifier;
    }

    public String getDeviceInformation() {
        return device_information;
    }

    public String getDeviceType() {
        return device_type;
    }

    public String getOnesignalPlayerId() {
        return onesignal_player_id;
    }
}
