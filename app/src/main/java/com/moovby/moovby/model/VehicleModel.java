package com.moovby.moovby.model;

import java.util.ArrayList;

public class VehicleModel {
    private String id, model_name, distance;
    private int rating, trips;
    private double latitude, longitude;
    private int model_id, hourly_rate;
    private boolean is_favorite;
    private Banner banner;
    private ArrayList<Images> images;
    private String description;
    private HostModel host;
    private Rates rates;

    public String getId() {
        return id;
    }

    public String getModelName() {
        return model_name;
    }

    public String getDistance() {
        return distance;
    }

    public int getRating() {
        return rating;
    }

    public int getTrips() {
        return trips;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getModel_id() {
        return model_id;
    }

    public int getHourlyRate() {
        return hourly_rate;
    }

    public boolean isIs_favorite() {
        return is_favorite;
    }

    public Banner getBanner() {
        return banner;
    }

    public ArrayList<Images> getImages() {
        return images;
    }

    public String getDescription() {
        return description;
    }

    public HostModel getHost() {
        return host;
    }

    public Rates getRates() {
        return rates;
    }

    public class Banner {
        private String banner_url, link;

        public String getBanner_url() {
            return banner_url;
        }

        public String getLink() {
            return link;
        }
    }

    public class Images {
        private int id;
        private String original, square;
        private boolean default_image;

        public int getId() {
            return id;
        }

        public String getOriginal() {
            return original;
        }

        public String getSquare() {
            return square;
        }

        public boolean isDefault_image() {
            return default_image;
        }
    }
}
