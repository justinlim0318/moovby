package com.moovby.moovby.model;

import java.util.ArrayList;

public class VehiclesResponseModel extends CommonResponseModel {
    private ArrayList<VehicleModel> vehicles;


    public ArrayList<VehicleModel> getVehicles() {
        return vehicles;
    }
}
