package com.moovby.moovby.model;

public class HostModel {
    private String name, first_name, last_name;
    private String avatar;
    private String about, lives;

    public String getName() {
        return name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getAbout() {
        return about;
    }

    public String getLives() {
        return lives;
    }
}
