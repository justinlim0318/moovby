package com.moovby.moovby.model;

public class SignInRequestModel {
    private String email, password;
    private DeviceInfo device_info;

    public SignInRequestModel(String email, String password) {
        this.email = email;
        this.password = password;
        this.device_info = new DeviceInfo("Device Identifier 1",
                "Device Information 1",
                "Android",
                "xxx");
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public DeviceInfo getDeviceInfo() {
        return device_info;
    }
}




