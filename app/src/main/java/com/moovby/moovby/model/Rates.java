package com.moovby.moovby.model;

public class Rates {
    private int hourly_rate, daily_rate, weekly_rate, monthly_rate;

    public int getHourlyRate() {
        return hourly_rate;
    }

    public int getDailyRate() {
        return daily_rate;
    }

    public int getWeeklyRate() {
        return weekly_rate;
    }

    public int getMonthlyRate() {
        return monthly_rate;
    }
}
