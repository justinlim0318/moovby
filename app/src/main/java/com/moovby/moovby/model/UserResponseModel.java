package com.moovby.moovby.model;

public class UserResponseModel {
    private String id, email;
    private String created_at, updated_at;
    private String auth_token, firebase_id;
    private boolean guest;
    private ProfileResponseModel profile;

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public String getFirebase_id() {
        return firebase_id;
    }

    public boolean isGuest() {
        return guest;
    }

    public ProfileResponseModel getProfile() {
        return profile;
    }

    public class ProfileResponseModel {
        private String id, first_name, last_name, avatar_url, phone_number, user_id;
        private String licence_url, identity_url, licence_number, identity_number;
        private boolean is_profile_verified;

        public String getId() {
            return id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public String getAvatar_url() {
            return avatar_url;
        }

        public String getPhone_number() {
            return phone_number;
        }

        public String getUser_id() {
            return user_id;
        }

        public String getLicence_url() {
            return licence_url;
        }

        public String getIdentity_url() {
            return identity_url;
        }

        public String getLicence_number() {
            return licence_number;
        }

        public String getIdentity_number() {
            return identity_number;
        }

        public boolean isIs_profile_verified() {
            return is_profile_verified;
        }
    }
}