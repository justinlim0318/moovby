package com.moovby.moovby.model;

public class CommonResponseModel {
    private boolean status;
    private String message;

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
