package com.moovby.moovby.model;

public class VehicleDetail {
    private String brand, model, year;
    private String mileage, transmission, type;
    private int doors, seats;
    private String pickup_instruction, return_instruction;

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getYear() {
        return year;
    }

    public String getMileage() {
        return mileage;
    }

    public String getTransmission() {
        return transmission;
    }

    public String getType() {
        return type;
    }

    public int getDoors() {
        return doors;
    }

    public int getSeats() {
        return seats;
    }

    public String getPickup_instruction() {
        return pickup_instruction;
    }

    public String getReturn_instruction() {
        return return_instruction;
    }
}

