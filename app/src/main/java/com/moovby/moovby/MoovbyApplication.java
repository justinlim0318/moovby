package com.moovby.moovby;

import android.app.Application;

import com.moovby.moovby.di.component.ApplicationComponent;
import com.moovby.moovby.di.component.DaggerApplicationComponent;
import com.moovby.moovby.di.module.ApiModule;

public class MoovbyApplication extends Application {

    private static ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
    }

    protected void initDagger() {
        applicationComponent = DaggerApplicationComponent
                .builder()
                .api(new ApiModule())
                .application(this)
                .build();
        applicationComponent.inject(this);
    }

    public static ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
