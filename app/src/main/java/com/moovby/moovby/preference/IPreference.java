package com.moovby.moovby.preference;

public interface IPreference {
    void setUserToken(String token);

    String getUserToken();
}
