package com.moovby.moovby.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.moovby.moovby.di.qualifier.ApplicationContext;
import com.moovby.moovby.di.scope.ApplicationScope;

import javax.inject.Inject;

@ApplicationScope
public class PreferenceHelper implements IPreference {
    private static final String MoovbyPref = "MoovbyPref";
    private final SharedPreferences mSharedPreferences;
    private final String USER_TOKEN = "USER_TOKEN";

    @Inject
    public PreferenceHelper(@ApplicationContext Context context) {
        mSharedPreferences = context.getSharedPreferences(MoovbyPref, Context.MODE_PRIVATE);
    }

    @Override
    public void setUserToken(String token) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(USER_TOKEN, token);
        edit.apply();
    }

    @Override
    public String getUserToken() {
        return mSharedPreferences.getString(USER_TOKEN, "");
    }
}
